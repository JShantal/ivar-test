<?php

namespace App\Http\Controllers;

use Auth;
use App\View;
use Illuminate\Http\Request;

class ViewController extends Controller
{
    /**
     * Add new view to storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        View::create([
            'ip' => $request->ip(),
            'user_id' => Auth::user()->id ?? NULL
        ]);
    }
}
