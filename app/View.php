<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class View extends Model
{
    protected $fillable = [
        'ip',
        'user_id',
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Disabling updated_at field
     *
     * @param mixed $value
     * @return void
     */
    public function setUpdatedAt($value)
    {
    }

}
