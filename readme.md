## Ivar Test Task from Ivan S.
Built on Laravel 5 using Bootstrap 4.

## Install
1. "git clone"
2. "cp .env.example .env" and fill the database and APP_KEY fields.
3. "composer install --no-dev"
4. "php artisan migrate"
