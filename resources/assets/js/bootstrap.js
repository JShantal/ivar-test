import Player from '@vimeo/player';

window._ = require('lodash');
window.Popper = require('popper.js').default;
window.$ = window.jQuery = require('jquery');
require('bootstrap');

window.axios = require('axios');
window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

let token = document.head.querySelector('meta[name="csrf-token"]');
window.axios.defaults.headers.common['X-CSRF-TOKEN'] = token.content;

let iframe = document.querySelector('iframe');
if (iframe) {
    let player = new Player(iframe);
    player.on('play', () => {
        axios.post('/views');
    });
    $('#vimeoModal').on('hide.bs.modal', () => {
        player.pause();
    });
}
