@extends('layouts.app')

@section('content')
    <div class="content d-flex justify-content-center">
        <div class="align-self-center">
            <button class="btn btn-primary" data-toggle="modal" data-target="#vimeoModal">Click me!</button>
        </div>
    </div>

    <div class="modal fade" id="vimeoModal" tabindex="-1" role="dialog" aria-hidden="true">
        <span class="modal-close" data-dismiss="modal">&times;</span>
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <iframe src="https://player.vimeo.com/video/269590912?title=0&byline=0&portrait=0&badge=0" width="640"
                        height="360" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
            </div>
        </div>
    </div>
@endsection
