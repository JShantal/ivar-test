<?php

Route::view('/', 'welcome');
Route::view('/home', 'home')->middleware('auth')->name('home');
Route::post('/views', 'ViewController')->name('views.store');
Auth::routes();
